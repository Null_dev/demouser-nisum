package com.demouser.demouser.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
@Entity
@JsonInclude
public class User {
	@Id
	private String id;
	@JsonProperty("name")
	private String name;

	@Email
	@Column(unique = true)
	private String email;
	@JsonProperty("password")
	private String password;

	private String token;

	@JsonProperty("isactive")
    private boolean active;

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}
	public String getToken() {
		return token;
	}
	public boolean isActive() {
		return active;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
}